superbuild_add_project(draco
  LICENSE_FILES
    LICENSE
  CMAKE_ARGS
    -DDRACO_BACKWARDS_COMPATIBILITY:BOOL=OFF
    -DDRACO_JS_GLUE:BOOL=OFF
)
