# Paraview Additional Superbuild Example

An example of a ParaView Additional Superbuild for a ParaView plugin, intended
to be an example on how to use the `SUPERBUILD_ADDITIONAL_FOLDERS` feature of the
ParaView-Superbuild in order to create a ParaView binary redistributable package
containing an additional plugin.

Specifically it shows how to build two additional projects. One is a standard project
that provides some libs, draco from https://github.com/google/draco.
The other is an additional ParaView plugin which is provided as an example here:
https://gitlab.kitware.com/paraview/paraview-ci-example.
Please note that the ParaView plugin does not actually depend on draco, this is just an example.

The complete documentation about this mechanism can be found [here](https://gitlab.kitware.com/paraview/paraview-superbuild/#superbuild-additional-folders).

This example has been tested with ParaView-superbuild at ad20024144967d370671e66e3a48439336d228e5, but it should
be compatible with all ParaView-superbuild version onwards.
The first released tag containing this feature should be ParaView v5.14.0.

It has been tested on Windows and Linux.
Developped and maintained by Kitware Europe.

# How to use
This additional superbuild cannot be built independantly and should be just pointed to during the
ParaView-superbuild configuration.

Linux:
 - Install git and developpement environnement
 - In a terminal:

```
git clone https://gitlab.kitware.com/paraview/paraview-additional-superbuild-example.git pase
git clone --recursive https://gitlab.kitware.com/paraview/paraview-superbuild pvsb
mkdir build
cd build
cmake -GNinja -DCMAKE_BUILD_TYPE=Release -DSUPERBUILD_ADDITIONAL_FOLDERS=../pase -DENABLE_qt5=ON -DENABLE_python3=ON -DENABLE_paraviewciexample=ON ../pvsb
ninja
ctest -R cpack
```

Windows:
 - Install ninja, git and a recent visual studio (>=2015)
 - Install Qt
 - Install NSIS to be able to generate an installer
 - In a visual studio x64 native command:
```
git clone https://gitlab.kitware.com/paraview/paraview-additional-superbuild-example.git pase
git clone --recursive https://gitlab.kitware.com/paraview/paraview-superbuild pvsb
mkdir build
cd build
cmake -GNinja -DCMAKE_BUILD_TYPE=Release -DSUPERBUILD_ADDITIONAL_FOLDERS=../pase -DENABLE_qt5=ON -DENABLE_python3=ON -DENABLE_paraviewciexample=ON ../pvsb
ninja
ctest -R cpack
```
This will produce an relocatable archive/installer containing ParaView with the new plugin bundled, that can be shared and installed anywhere.

# How to adapt to your own projects and plugins

You should be able to copy this repository and replace project files by your own files for your dependencies and plugins,
exactly as if adding a project in the ParaView superbuild.

You will also want to modify `package.cmake` to add your own plugins there.
You could even add a `bundle.cmake` in order to specify a custom bundling logic
to ship your own executables and dynamically loaded libraries if needed, using
superbuild macros.

# License and Copyright

Copyright (c) 2022 Kitware SAS.
Like ParaView, This Paraview Additional Superbuild Example is distributed under the OSI-approved BSD
3-clause License. See [Copyright.txt][] for details.

[Copyright.txt]: Copyright.txt
