superbuild_set_revision(draco
  URL     "https://github.com/google/draco/archive/refs/tags/1.5.6.tar.gz"
  URL_MD5 dbe3a9e286ee5b79016470349d78b2a3)

superbuild_set_selectable_source(paraviewciexample
  SELECT git CUSTOMIZABLE DEFAULT
  GIT_REPOSITORY "https://gitlab.kitware.com/paraview/paraview-ci-example.git"
  GIT_TAG        "origin/main"
  )
